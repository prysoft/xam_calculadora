﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculadora1
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            _ListapkOper();
        }

        void _ListapkOper()
        {
            pkOper.Items.Add("Suma");
            pkOper.Items.Add("Resta");
            pkOper.Items.Add("Multiplicacion");
            pkOper.Items.Add("Division");
        }

        private void pkOper_SelectedIndexChanged(object sender, EventArgs e)
        {
            string op = pkOper.SelectedItem.ToString();
            long n1 = Int64.Parse(txtN1.Text);
            long n2 = Int64.Parse(txtN2.Text);

            double res = 0;

            if (op.Substring(0,1) == "S")
            {
                res = n1 + n2;
            }
            if (op.Substring(0,1) == "R")
            {
                res = n1 - n2;
            }
            if (op.Substring(0,1) == "M")
            {
                res = n1 * n2;
            }
            if (op.Substring(0,1) == "D")
            {
                res = n1 / n2;
            }

            lblRes.Text = res.ToString();
        }

    }
}
